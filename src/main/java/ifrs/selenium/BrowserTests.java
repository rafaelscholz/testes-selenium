package ifrs.selenium;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.junit.Assert;
import org.junit.Test;

public class BrowserTests {
	
	public String DRIVER_PATH = System.getProperty("user.dir") + 
			"/src/main/resources/webdrivers/geckodriver";
	
	@Test
	public void checkIfrsFelizIsNotDown() {
		System.setProperty("webdriver.gecko.driver", this.DRIVER_PATH);
		
		FirefoxDriver browser = new FirefoxDriver();	
		browser.manage().window().setSize(new Dimension(1000, 1000));
		
		browser.get("https://feliz.ifrs.edu.br");
		String title = browser.getTitle();
		browser.quit();
		
		String correctTitle = "Campus Feliz - Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul";
		Assert.assertEquals(correctTitle, title);
	}
}
